/**
 * @license GPL-2.0-or-later
 * Copyright (c) 2018 Michael Koch <m.koch@emkay443.de>
 *
 * This file is part of JStris.
 * JStris is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * JStris is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JStris.  If not, see <http://www.gnu.org/licenses/>.
 */

const colors = {
	 yellow: "#ffff00",
	 turquoise: "#00ffff",
	 red: "#ff0000",
	 green: "#00ff00",
	 blue: "#0000ff",
	 orange: "#ff7700",
	 purple: "#8c1aff",
	 darkgreen: "#006600",
	 darkblue: "#000066",
	 darkred: "#660000",
	 darkyellow: "#666600",
	 darkpurple: "#330066",
	 pink: "#ff99cc",
	 bronze: "#996633",
	 silver: "#b3b3b3",
	 gold: "#ffb31a",
	 gray: "#595959",
	 darkgray: "#262626"
}

const original_tiles = [
	{
		name: "square",
		layout: [
			[1,1],
			[1,1]
		],
		dimensions: {
			x: 2,
			y: 2
		},
		color: colors.yellow
	},
	{
		name: "line",
		layout: [
			[1],
			[1],
			[1],
			[1]
		],
		dimensions: {
			x: 1,
			y: 4
		},
		color: colors.turquoise
	},
	{
		name: "squiggly_left",
		layout: [
			[1,1,0],
			[0,1,1]
		],
		dimensions: {
			x: 3,
			y: 2
		},
		color: colors.red
	},
	{
		name: "squiggly_right",
		layout: [
			[0,1,1],
			[1,1,0]
		],
		dimensions: {
			x: 3,
			y: 2
		},
		color: colors.green
	},
	{
		name: "l_left",
		layout: [
			[1,0,0],
			[1,1,1]
		],
		dimensions: {
			x: 3,
			y: 2
		},
		color: colors.blue
	},
	{
		name: "l_right",
		layout: [
			[0,0,1],
			[1,1,1]
		],
		dimensions: {
			x: 3,
			y: 2
		},
		color: colors.orange
	},
	{
		name: "t",
		layout: [
			[0,1,0],
			[1,1,1]
		],
		dimensions: {
			x: 3,
			y: 2
		},
		color: colors.purple
	}
 ];

 const bonus_tiles = [
	 {
		 name: "1337",
		 layout: [
			 [0,1,0],
			 [0,0,1],
			 [1,1,1]
		 ],
		 dimensions: {
			 x: 3,
			 y: 3
		 },
		 color: colors.darkgray
	 },
	 {
		 name: "u",
		 layout: [
			 [1,0,1],
			 [1,1,1]
		 ],
		 dimensions: {
			 x: 3,
			 y: 2
		 },
		 color: colors.gold
	 },
	 {
		 name: "plus",
		 layout: [
			 [0,1,0],
			 [1,1,1],
			 [0,1,0]
		 ],
		 dimensions: {
			 x: 3,
			 y: 3
		 },
		 color: colors.darkpurple
	 },
	 {
		 name: "s",
		 layout: [
			 [0,1,1],
			 [0,1,0],
			 [1,1,0]
		 ],
		 dimensions: {
			 x: 3,
			 y: 3
		 },
		 color: colors.pink
	 },
	 {
		 name: "corner",
		 layout: [
			 [0,1],
			 [1,1]
		 ],
		 dimensions: {
			 x: 2,
			 y: 2
		 },
		 color: colors.silver
	 },
	 {
		 name: "p",
		 layout: [
			 [1,1],
			 [1,1],
			 [1,0]
		 ],
		 dimensions: {
			 x: 2,
			 y: 3
		 },
		 color: colors.bronze
	 },
	 {
		 name: "t_large",
		 layout: [
			 [1,1,1],
			 [0,1,0],
			 [0,1,0]
		 ],
		 dimensions: {
			 x: 3,
			 y: 3
		 },
		 color: colors.bronze
	 },
	 {
		 name: "f",
		 layout: [
			 [0,1,1],
			 [1,1,0],
			 [0,1,0]
		 ],
		 dimensions: {
			 x: 3,
			 y: 3
		 },
		 color: colors.darkblue
	 },
	 {
		 name: "z",
		 layout: [
			 [1,1,0],
			 [0,1,0],
			 [0,1,1]
		 ],
		 dimensions: {
			 x: 3,
			 y: 3
		 },
		 color: colors.darkred
	 }
 ];

 var tiles = original_tiles;