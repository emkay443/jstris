/**
 * Keyboard input with customisable repeat (set to 0 for no key repeat)
 *
 * Original author: bonbince (https://stackoverflow.com/users/18936/bobince)
 * Improved by: Michael Koch <m.koch@emkay443.de>
 * Source: https://stackoverflow.com/questions/3691461/remove-key-press-delay-in-javascript/3691661#3691661
 */

function KeyboardController(keys) {
	// Lookup of key codes to timer ID, or null for no repeat
	//
	var timers = {};
	var timeouts = {};

	// When key is pressed and we don't already think it's pressed, call the
	// key action callback and set a timer to generate another one after a delay
	//
	document.onkeydown = function(event) {
		var key = (event || window.event).keyCode;
		if (! (key in keys)) {
			return true;
		}
		if (! (key in timers)) {
			timers[key] = null;
			keys[key]["fun"]();
			if (keys[key]["repeatFirst"] !== undefined && keys[key]["repeatFirst"] !== null && keys[key]["repeatFirst"] !== 0 &&
				keys[key]["repeatAfter"] !== undefined && keys[key]["repeatAfter"] !== null && keys[key]["repeatAfter"] !== 0) {
				timeouts[key] = setTimeout(function() {
					keys[key]["fun"];
					timers[key] = setInterval(keys[key]["fun"], keys[key]["repeatAfter"]);
				}, keys[key]["repeatFirst"]);
			} else if (keys[key]["repeat"] !== undefined && keys[key]["repeat"] !== null && keys[key]["repeat"] !== 0) {
				timers[key] = setInterval(keys[key]["fun"], keys[key]["repeat"]);
			}
		}
		return false;
	};

	// Cancel timeout and mark key as released on keyup
	//
	document.onkeyup = function(event) {
		var key = (event || window.event).keyCode;
		if (key in timers) {
			if (timers[key] !== null) {
				clearInterval(timers[key]);
			}
			if (timeouts[key] !== null) {
				clearTimeout(timeouts[key]);
			}
			if ("kup" in keys[key]) {
				keys[key]["kup"]();
			}
			delete timers[key];
		}
	};

	// When window is unfocused we may not get key events. To prevent this
	// causing a key to 'get stuck down', cancel all held keys
	//
	window.onblur = function() {
		for (key in timers) {
			if (timers[key] !== null) {
				clearInterval(timers[key]);
			}
		}
		timers= {};
	};
}