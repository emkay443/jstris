/**
 * @license GPL-2.0-or-later
 * Copyright (c) 2018 Michael Koch <m.koch@emkay443.de>
 *
 * This file is part of JStris.
 * JStris is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * JStris is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JStris.  If not, see <http://www.gnu.org/licenses/>.
 */

var sfx_priority = 0;
const sfx = {
	rotate: "includes/sound/sfx/rotate.wav",
	pause: "includes/sound/sfx/pause.wav",
	drop: "includes/sound/sfx/drop.wav",
	lineclear: "includes/sound/sfx/lineclear.wav",
	jstris: "includes/sound/sfx/jstris.wav",
	gameover: "includes/sound/sfx/gameover.wav"
}

function initializeSound() {

	$("#sfx").empty();
	$.each(sfx, function(key, value) {
		$("#sfx").append("<audio id='sfx_" + key + "' preload='auto' src='" + value + "'></audio>");
	});
}

function play_sfx(src) {
	var audio = $("#sfx_" + src)[0];
	var priority = arguments.length > 1 && arguments[1] !== undefined && arguments[1] !== null && typeof arguments[1] === "number" ? arguments[1] : 0;

	if (priority >= sfx_priority) {
		sfx_priority = priority;
		audio.pause();
		audio.currentTime = 0;
		audio.onended = function() {
			sfx_priority = 0;
		}
		audio.play();
	}
}