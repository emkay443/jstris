/**
 * @license GPL-2.0-or-later
 * Copyright (c) 2018 Michael Koch <m.koch@emkay443.de>
 *
 * This file is part of JStris.
 * JStris is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * JStris is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JStris.  If not, see <http://www.gnu.org/licenses/>.
 */

var tile_size = 30;
const area_width = 10;
const area_height = 20;

var area = [];
var current_tile = {};
var next_tile = {};
var gameover = false;
var gamestarted = false;
var score = 0;
var lines = 0;
var jstris = 0;
var level = 0;
var highscore = 0;
var falling_timeout;
var difficulty = 2;
var paused = false;
var bonustiles = false;

function init() {

	clearBoard();
	setStyling();
	initializeSound();
	window.addEventListener("resize", setStyling);
	$("#highscore").html(localStorage.getItem("jstris_highscore") !== null ? localStorage.getItem("jstris_highscore") : 0);

	KeyboardController({
		37: {
			fun: function() { moveCurrentTile("left"); },
			repeatFirst: 266,
			repeatAfter: 100
		},
		65: {
			fun: function() { moveCurrentTile("left"); },
			repeatFirst: 266,
			repeatAfter: 100
		},
		38: {
			fun: function() { moveCurrentTile("rotateRight"); },
			repeat: 500
		},
		87: {
			fun: function() { moveCurrentTile("rotateRight"); },
			repeat: 500
		},
		81: {
			fun: function() { moveCurrentTile("rotateLeft"); },
			repeat: 500
		},
		69: {
			fun: function() { moveCurrentTile("rotateRight"); },
			repeat: 500
		},
		39: {
			fun: function() { moveCurrentTile("right"); },
			repeatFirst: 266,
			repeatAfter: 100
		},
		68: {
			fun: function() { moveCurrentTile("right"); },
			repeatFirst: 266,
			repeatAfter: 100
		},
		40: {
			fun: function() { moveCurrentTile("down"); },
			repeat: 50
		},
		83: {
			fun: function() { moveCurrentTile("down"); },
			repeat: 50
		},
		32 : {
			fun: function() { moveCurrentTile("harddrop"); }
		},
		80: {
			fun: function() { togglePause(); }
		},
		19: {
			fun: function() { togglePause(); }
		},
		27: {
			fun: function() { reset(); }
		}
	});
}

function togglePause() {
	var canvas = $("#gamearea")[0];
	var ctx = canvas.getContext('2d');

	if (gamestarted) {
		if (paused) {
			paused = false;
			redrawBoard();
			falling_timeout = window.setTimeout(function() {
				moveCurrentTile("down", true);
			}, 1000 / (level / difficulty + 1));
		} else {
			paused = true;
			clearTimeout(falling_timeout);
			redrawBoard();
		}
		play_sfx('pause', 2);
	}
}

function toggleBonusTiles() {

	if (bonustiles) {
		tiles = original_tiles;
		bonustiles = false;
		$("#bonustiles").html("Extra blocks: OFF");
	} else {
		tiles = original_tiles.concat(bonus_tiles);
		bonustiles = true;
		$("#bonustiles").html("Extra blocks: ON");
	}
}

function setStyling() {

	if (document.documentElement.clientWidth < 350 || document.documentElement.clientHeight < 400) {
		tile_size = 18;
	} else if (document.documentElement.clientWidth < 400 || document.documentElement.clientHeight < 600) {
		tile_size = 20;
	} else if (document.documentElement.clientWidth < 500 || document.documentElement.clientHeight < 700) {
		tile_size = 22;
	} else {
		tile_size = 30;
	}

	if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$("#mobilebuttons").css("display", "grid");
	} else {
		$("#mobilebuttons").css("display", "none");
	}

	$("#gamearea").css("width", tile_size * area_width);
	$("#gamearea").css("height", tile_size * area_height);
	$("#gamearea")[0].width = tile_size * area_width;
	$("#gamearea")[0].height = tile_size * area_height;
	$("#scoreboard").css("left", tile_size * area_width + 40);
	$("#scoreboard").css("width", tile_size * 4 + 10);
	$("#scoreboard")[0].width = tile_size * 4 + 10;
	$("#preview").css("left", tile_size * area_width + 40);
	$("#preview").css("top", tile_size * area_width + 90);
	$("#preview").css("width", tile_size * 4 + 10);
	$("#preview").css("height", tile_size * 4 + 10);
	$("#preview")[0].width = tile_size * 4 + 10;
	$("#preview")[0].height = tile_size * 4 + 10;
	$("body").css("font-size", Math.floor(tile_size / 1.5) + "px")
	$("button").css("font-size", Math.floor(tile_size / 1.7) + "px")

	redrawBoard();
}

function reset() {
	var i, j;
	var ctx = $("#gamearea")[0].getContext('2d');

	clearBoard();
	redrawBoard();
	clearTimeout(falling_timeout);

	score = 0;
	lines = 0;
	level = 0;
	gameover = false;
	gamestarted = true;

	current_tile = {};
	next_tile = {};

	$("#score").html(score);
	$("#lines").html(lines);
	$("#level").html(level);

	summonRandomTile();
}

function clearBoard() {

	area = [];
	for (j = 0; j < area_width; j++) {
		var line = [];
		for (i = 0; i < area_height; i++) {
			line.push({
				color: "",
				occupied: false
			});
		}
		area.push(line);
	}
}

function summonRandomTile() {
	var i, j;

	if (! $.isEmptyObject(next_tile)) {
		current_tile = next_tile;
	} else {
		current_tile = JSON.parse(JSON.stringify(tiles[Math.floor(Math.random() * tiles.length)]));
	}
	next_tile = JSON.parse(JSON.stringify(tiles[Math.floor(Math.random() * tiles.length)]));
	redrawPreview();
	current_tile.position = {
		x: Math.floor(area_width / 2 - (current_tile.dimensions.x / 2)),
		y: 0
	}
	for (j = 0; j < current_tile.dimensions.y; j++) {
		for (i = 0; i < current_tile.dimensions.x; i++) {
			if (area[current_tile.position.x + i][current_tile.position.y + j].occupied) {
				gameover = true;
				play_sfx('gameover', 3);
			}
			if (current_tile.layout[j][i] === 1) {
				area[current_tile.position.x + i][current_tile.position.y + j] = {
					color: current_tile.color,
					occupied: true
				}
			}
		}
	}
	redrawBoard();
	if (! gameover) {
		setFallingTimeout();
	}
}

function setFallingTimeout() {
	falling_timeout = window.setTimeout(function() {
		moveCurrentTile("down", true);
	}, 1000 / (level / difficulty + 1));
}

function redrawBoard() {
	var i, j;
	var canvas = $("#gamearea")[0];
	var ctx = canvas.getContext('2d');

	ctx.clearRect(0, 0, canvas.width, canvas.height);
	canvas.width = canvas.width;
	if (paused) {
		ctx.beginPath();
		ctx.font = tile_size + "px Arial";
		ctx.textAlign = "center";
		ctx.strokeStyle = "#000000";
		ctx.lineWidth = Math.max(2, Math.floor(tile_size / 10));
		ctx.strokeText("PAUSE", Math.floor(tile_size * area_width / 2), Math.floor(tile_size * area_height / 2));
		ctx.fillStyle = "#ffffff";
		ctx.fillText("PAUSE", Math.floor(tile_size * area_width / 2), Math.floor(tile_size * area_height / 2));
		ctx.closePath();
	} else {
		for (j = 0; j < area_height; j++) {
			for (i = 0; i < area_width; i++) {
				if (area[i][j].occupied) {
					ctx.beginPath();
					ctx.rect(tile_size * i, tile_size * j, tile_size, tile_size);
					ctx.fillStyle = area[i][j].color;
					ctx.fill();
					ctx.lineWidth = 1;
					ctx.strokeStyle = "#000000";
					ctx.stroke();
					ctx.closePath();
				}
			}
		}
		if (gameover) {
			ctx.beginPath();
			ctx.font = tile_size + "px Arial";
			ctx.textAlign = "center";
			ctx.strokeStyle = "#000000";
			ctx.lineWidth = Math.max(2, Math.floor(tile_size / 10));
			ctx.strokeText("GAME OVER!", Math.floor(tile_size * area_width / 2), Math.floor(tile_size * area_height / 2));
			ctx.fillStyle = "#ffffff";
			ctx.fillText("GAME OVER!", Math.floor(tile_size * area_width / 2), Math.floor(tile_size * area_height / 2));
			ctx.closePath();
		}
	}
}

function redrawPreview() {
	var i, j;
	var canvas = $("#preview")[0];
	var ctx = canvas.getContext('2d');
	var n = next_tile;

	ctx.clearRect(0, 0, canvas.width, canvas.height);
	canvas.width = canvas.width;
	for (j = 0; j < n.dimensions.y; j++) {
		for (i = 0; i < n.dimensions.x; i++) {
			if (n.layout[j][i] === 1) {
				ctx.beginPath();
				ctx.rect(tile_size * i, tile_size * j, tile_size, tile_size);
				ctx.fillStyle = n.color;
				ctx.fill();
				ctx.lineWidth = 1;
				ctx.strokeStyle = "#000000";
				ctx.stroke();
				ctx.closePath();
			}
		}
	}
}

function updateScore() {
	var i, j, k;
	var l = 0;

	for (j = 0; j < area_height; j++) {
		var fullLine = true;
		for (i = 0; i < area_width && fullLine; i++) {
			if (! area[i][j].occupied) {
				fullLine = false;
			}
		}
		if (fullLine) {
			l++;
			for (i = 0; i < area_width; i++) {
				area[i][j] = {
					occupied: false,
					color: ""
				}
			}
			for (k = j; k > 0; k--) {
				for (i = 0; i < area_width; i++) {
					area[i][k] = JSON.parse(JSON.stringify(area[i][k-1]));
				}
			}
		}
	}
	if (l > 3) {
		play_sfx('jstris', 1);
		jstris++;
	} else if (l > 0) {
		play_sfx('lineclear', 1);
	}
	lines += l;
	switch(l) {
		case 1:
			score += 40 * (level + 1);
			break;
		case 2:
			score += 100 * (level + 1);
			break;
		case 3:
			score += 300 * (level + 1);
			break;
		case 4:
			score += 1200 * (level + 1);
			break;
	}

	level = Math.floor(lines / 10);

	$("#score").html(score);
	$("#lines").html(lines);
	$("#jstris").html(jstris);
	$("#level").html(level);

	if (score > highscore) {
		highscore = score;
		localStorage.setItem("jstris_highscore", highscore);
		$("#highscore").html(highscore);
	}
}

function clearCurrentTile() {
	for (j = 0; j < current_tile.dimensions.y; j++) {
		for (i = 0; i < current_tile.dimensions.x; i++) {
			if (current_tile.layout[j][i] === 1) {
				area[current_tile.position.x + i][current_tile.position.y + j] = {
					color: "",
					occupied: false
				};
			}
		}
	}
}

function drawCurrentTile() {
	for (j = 0; j < current_tile.dimensions.y; j++) {
		for (i = 0; i < current_tile.dimensions.x; i++) {
			if (current_tile.layout[j][i] === 1) {
				area[current_tile.position.x + i][current_tile.position.y + j] = {
					color: current_tile.color,
					occupied: true
				};
			}
		}
	}
}

function moveCurrentTile(action) {
	var i, j;
	var c = current_tile;
	var move = true;
	var repeat = arguments.length > 1 && arguments[1] !== undefined && arguments[1] !== null && typeof arguments[1] === "boolean" ? arguments[1] : false;

	if (! $.isEmptyObject(c) && ! gameover) {
		switch(action) {
			case "down":
				clearTimeout(falling_timeout);
				for (j = 0; j < c.dimensions.y && move; j++) {
					for (i = 0; i < c.dimensions.x && move; i++) {
						if (c.layout[j][i] === 1) {
							if (c.position.y + j + 1 >= area_height || (area[c.position.x + i][c.position.y + j + 1].occupied && ! (j < c.dimensions.y - 1 && c.layout[j + 1][i] === 1))) {
								move = false;
								play_sfx('drop');
								updateScore();
								summonRandomTile();
							}
						}
					}
				}
				if (! move) break;
				clearCurrentTile();
				c.position.y++;
				drawCurrentTile();
				setFallingTimeout();
				break;
			case "left":
				for (j = 0; j < c.dimensions.y && move; j++) {
					for (i = 0; i < c.dimensions.x && move; i++) {
						if (c.layout[j][i] === 1) {
							if (c.position.x + i - 1 < 0 || (i > 0 && c.layout[j][i - 1] !== 1 && area[c.position.x + i - 1][c.position.y + j].occupied) || (i === 0 && area[c.position.x + i - 1][c.position.y + j].occupied)) {
								move = false;
								updateScore();
							}
						}
					}
				}
				if (!move) break;
				clearCurrentTile();
				c.position.x--;
				drawCurrentTile();
				break;
			case "right":
				for (j = 0; j < c.dimensions.y && move; j++) {
					for (i = 0; i < c.dimensions.x && move; i++) {
						if (c.layout[j][i] === 1) {
							if (c.position.x + i + 1 >= area_width || (i < c.dimensions.x - 1 && c.layout[j][i + 1] !== 1 && area[c.position.x + i + 1][c.position.y + j].occupied) || (i === c.dimensions.x - 1 && area[c.position.x + i + 1][c.position.y + j].occupied)) {
								move = false;
								updateScore();
							}
						}
					}
				}
				if (!move) break;
				clearCurrentTile();
				c.position.x++;
				drawCurrentTile();
				break;
			case "rotateRight":
				clearCurrentTile();
				var test = JSON.parse(JSON.stringify(c));
				var rotate = true;
				test.layout = _.zip.apply(_, test.layout);
				for (i = 0; i < test.layout.length; i++) {
					test.layout[i].reverse();
				}
				test.dimensions.x = test.layout[0].length;
				test.dimensions.y = test.layout.length;
				if (test.dimensions.x + test.position.x > area_width) {
					test.position.x--;
				}
				for (j = 0; j < test.dimensions.y && rotate; j++) {
					for (i = 0; i < test.dimensions.x && rotate; i++) {
						if (test.layout[j][i] === 1) {
							if (area[test.position.x + i][test.position.y + j].occupied) {
								rotate = false;
							}
						}
					}
				}
				if (rotate) {
					current_tile = JSON.parse(JSON.stringify(test));
					play_sfx('rotate');
				}
				drawCurrentTile();
				break;
			case "rotateLeft":
				clearCurrentTile();
				var test = JSON.parse(JSON.stringify(c));
				var rotate = true;
				for (i = 0; i < test.layout.length; i++) {
					test.layout[i].reverse();
				}
				test.layout = _.zip.apply(_, test.layout);
				test.dimensions.x = test.layout[0].length;
				test.dimensions.y = test.layout.length;
				if (test.dimensions.x + test.position.x > area_width) {
					test.position.x--;
				}
				for (j = 0; j < test.dimensions.y && rotate; j++) {
					for (i = 0; i < test.dimensions.x && rotate; i++) {
						if (test.layout[j][i] === 1) {
							if (area[test.position.x + i][test.position.y + j].occupied) {
								rotate = false;
							}
						}
					}
				}
				if (rotate) {
					current_tile = JSON.parse(JSON.stringify(test));
					play_sfx('rotate');
				}
				drawCurrentTile();
				break;
			case "harddrop":
				clearTimeout(falling_timeout);
				while (move) {
					for (j = 0; j < c.dimensions.y && move; j++) {
						for (i = 0; i < c.dimensions.x && move; i++) {
							if (c.layout[j][i] === 1) {
								if (c.position.y + j + 1 >= area_height || (area[c.position.x + i][c.position.y + j + 1].occupied && ! (j < c.dimensions.y - 1 && c.layout[j + 1][i] === 1))) {
									move = false;
									updateScore();
									summonRandomTile();
								}
							}
						}
					}
					if (! move) break;
					clearCurrentTile();
					c.position.y++;
					drawCurrentTile();
				}
				play_sfx('drop');
				break;
		}
		redrawBoard();
	}
}

if (! String.prototype.format) {
	String.prototype.format = function() {
		var args = arguments;
		return this.replace(/{(\d+)}/g, function(match, number) {
			return typeof args[number] !== 'undefined' ? args[number] : match;
		});
	};
}